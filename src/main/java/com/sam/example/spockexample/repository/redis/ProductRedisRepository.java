package com.sam.example.spockexample.repository.redis;

import com.sam.example.spockexample.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRedisRepository extends CrudRepository<Product, String> {
}
