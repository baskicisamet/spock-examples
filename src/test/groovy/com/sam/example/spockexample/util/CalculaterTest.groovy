package com.sam.example.spockexample.util

import spock.lang.Specification


class CalculaterTest extends Specification {

    def "Adding two numbers to return the sum" (){

        when: "a calculater is created"
        def calculater = new Calculater()

        then: "1 plus 1 equals 2"
        calculater.add(1,1) == 3

    }

}