package com.sam.example.spockexample.model;

import org.springframework.stereotype.Component;

@Component
public class TestComponent {

    String hello(){
        return "hello";
    }
}
