package com.sam.example.spockexample.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;


@Builder
@Data
@RedisHash("product")
public class Product {

    @Id
    @Indexed
    private String id;
    private  String name;
    private String brand;
    private String model;


}
