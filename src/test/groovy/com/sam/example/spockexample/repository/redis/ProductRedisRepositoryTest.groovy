package com.sam.example.spockexample.repository.redis

import com.github.dockerjava.api.command.CreateContainerCmd
import com.github.dockerjava.api.model.ExposedPort
import com.github.dockerjava.api.model.PortBinding
import com.github.dockerjava.api.model.Ports
import com.sam.example.spockexample.model.Product
import com.sam.example.spockexample.model.TestComponent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.testcontainers.containers.GenericContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

import java.util.function.Consumer

@SpringBootTest
@Testcontainers
class ProductRedisRepositoryTest extends Specification {

    @Autowired
    ProductRedisRepository productRedisRepository

    @Autowired
    TestComponent testComponent


    static Consumer<CreateContainerCmd> cmd = { e -> e.withPortBindings(new PortBinding(Ports.Binding.bindPort(6379), new ExposedPort(6379)))}

    @Shared
    public GenericContainer redis =
            new GenericContainer("redis:3.0.2")
                    //.withExposedPorts(6379)
                    .withCreateContainerCmdModifier(cmd)

    def "check redis repository save and get"(){

        given:
            String id = "id";
            Product product = Product.builder()
                    .brand("brand")
                    .id(id)
                    .model("model")
                    .name( "name")
                    .build()
        when:
            productRedisRepository.save(product)
            Product persistProduct = productRedisRepository.findById(id).get()

        then:
            persistProduct.getName() == product.getName()
    }

}
